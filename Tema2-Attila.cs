using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tema2
{

    //    --------------------
    // 1.Write a C# program to print the odd numbers from 1 to 99. Prints one number per line. 
    //{
    //    class OddNum
    //    {

    //        static void Main(string[] args)
    //        {

    //            for (int i = 0; i <= 100; i++)
    //                if (i % 2 != 0)
    //                {
    //                    Console.WriteLine(i);
    //                }
    //            Console.ReadLine();
    //        }
    //    }
    //}

    //    --------------------
    // 3.Write a C# Sharp program to find whether a given year is a leap year or not.

    //class leapyearfinder
    //{
    //    static void Main(string[] args)
    //    {
    //        int year;
    //        Console.WriteLine("enter year to check: ");
    //        year = Convert.ToInt32(Console.ReadLine());
    //        if ((year % 4 == 0 && year % 100 != 0) || (year % 400 == 0))
    //        {
    //            Console.WriteLine(year + " is a leap year! ");
    //        }
    //        else
    //        {
    //            Console.WriteLine(year + " is not a leap year! ");
    //        }
    //Console.ReadKey();
    //    }

    //}

    //-----------------
    // 2.Write a shot program that reads a number n and than a list that contains n numbers. 
    // Find the minimum and maximum of this list and print it out on the console. 
    // Handle the cases when a different value than a number is added in the list, so the program doesn�t crash.

    class MinMaxFinder
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter length of list:");
            int listLength = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Length of list will be: " + listLength);


            List<int> numbers = new List<int>();


            for (int i = 0; i < listLength; i++)
            {
                Console.WriteLine("Enter a number");
                int inputNumber;
                string input = Console.ReadLine();
                bool isString = int.TryParse(input, out inputNumber);
                numbers.Add(inputNumber);
            }

            int min;
            int max;

            for (int i = 0; i < numbers.Count; i++)

            {

                Console.WriteLine(numbers[i]);
            }
            min = numbers[0];
            max = numbers[0];
            for (int i = 1; i < numbers.Count; i++)
            {

                if (min > numbers[i])
                {
                    min = numbers[i];
                }
                if (max < numbers[i])
                {
                    max = numbers[i];
                }

            }

            Console.WriteLine("Maximum: " + max);
            Console.WriteLine("Minimum: " + min);
            Console.WriteLine("Press a key to close");
            Console.ReadKey();
        }

    }
}

